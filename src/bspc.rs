use std::io::Write;
use std::os::unix::net::UnixStream;

pub struct Bspc {
    socket: &'static str,
}

impl Bspc {
    pub fn new(socket: &'static str) -> Self {
        Self { socket }
    }

    pub fn hide(&mut self, id: &str) {
        self.toggle(id, "on");
    }

    pub fn show(&mut self, id: &str) {
        self.toggle(id, "off");
    }

    fn toggle(&mut self, id: &str, mode: &str) {
        let data = format!(
            "node\x00{}\x00--flag\x00sticky={}\x00--flag\x00hidden={}\x00--focus\x00",
            id, mode, mode
        );

        UnixStream::connect(self.socket)
            .expect("Failed to connect")
            .write_all(data.as_bytes())
            .expect("Failed to write to the socket");
    }
}
