use crate::bspc::Bspc;
use crate::registry::Registry;

pub struct Stash {
    registry: Registry,
    bspc: Bspc,
}

impl Stash {
    pub fn new(registry: Registry, bspc: Bspc) -> Self {
        Self { registry, bspc }
    }

    pub fn push(&mut self, id: &str) {
        self.bspc.hide(id);
        self.registry.push(id);
    }

    pub fn pop(&mut self) {
        if let Some(id) = self.registry.pop() {
            self.bspc.show(&id)
        }
    }

    pub fn shift(&mut self) {
        if let Some(id) = self.registry.shift() {
            self.bspc.show(&id)
        }
    }

    pub fn swap_with_last(&mut self, id: &str) {
        if let Ok(last_id) = self.registry.swap_with_last(id) {
            self.bspc.show(&last_id);
            self.bspc.hide(id);
        }
    }
}
