mod bspc;
mod cli;
mod dzen2;
mod registry;
mod stash;

use bspc::Bspc;
use cli::Action;
use registry::Registry;
use stash::Stash;

fn main() {
    let action = match cli::parse_args() {
        Ok(action) => action,
        Err(err) => {
            eprintln!("ERROR: {}\n{}", err, cli::HELP);
            std::process::exit(1);
        }
    };

    let bspc = Bspc::new("/tmp/bspwm_0_0-socket");
    let registry = Registry::new("/tmp/window-stash");
    let mut stash = Stash::new(registry, bspc);

    match action {
        Action::SwapLastWith(id) => stash.swap_with_last(&id),
        Action::Stash(id) => stash.push(&id),
        Action::RevealLast => stash.pop(),
        Action::RevealFirst => stash.shift(),
    }
}
