use std::fs::{self, File};
use std::io::Write;
use std::process::{Command, Stdio};

const PID: &str = "/tmp/.dzen2-window-stash";

pub fn on() -> Result<(), Box<dyn std::error::Error>> {
    let dzen2 = Command::new("dzen2")
        .stdin(Stdio::piped())
        .stdout(Stdio::null())
        .args(&["-p", "-w", "5", "-h", "5", "-x", "2555", "-y", "1435"])
        .spawn()?;

    let mut f = File::create(PID)?;
    f.write_all(dzen2.id().to_string().as_bytes())?;
    dzen2.stdin.unwrap().write_all(b"^r(5x5)\n")?;
    Ok(())
}

pub fn off() -> Result<(), Box<dyn std::error::Error>> {
    let pid: i32 = fs::read_to_string(PID)?.parse()?;
    unsafe { libc::kill(pid, 1) };
    fs::remove_file(PID)?;
    Ok(())
}
