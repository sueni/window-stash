pub const HELP: &str = "
USAGE:
  window-stash [FLAGS] [OPTIONS]

FLAGS:
  -h                      Show help
      --reveal-last       Reveal the most recently stashed window
      --reveal-first      Reveal the less recently stashed window

OPTIONS:
      --stash <ID>       Stash the window by id
      --swap-last-with    Swap the last stashed window with the window by id
";

pub enum Action {
    SwapLastWith(String),
    Stash(String),
    RevealLast,
    RevealFirst,
}

pub fn parse_args() -> Result<Action, lexopt::Error> {
    use lexopt::prelude::*;

    let mut action = Action::RevealLast;
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Long("stash") => {
                let id = parser.value()?.into_string()?;
                action = Action::Stash(id);
            }

            Long("swap-last-with") => {
                let id = parser.value()?.into_string()?;
                action = Action::SwapLastWith(id);
            }

            Long("reveal-last") => action = Action::RevealLast,
            Long("reveal-first") => action = Action::RevealFirst,

            Short('h') => {
                print!("{}", HELP);
                std::process::exit(0);
            }
            _ => return Err(arg.unexpected()),
        }
    }
    Ok(action)
}
