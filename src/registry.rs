use crate::dzen2;
use std::fs::OpenOptions;
use std::io::{self, BufRead, BufReader, BufWriter, Read, Seek, SeekFrom, Write};
use std::path::PathBuf;

const IDLEN: u64 = 10;
const IDSHIFT: i64 = -(IDLEN as i64 + 1);

pub struct Registry {
    storage: PathBuf,
}

impl Registry {
    pub fn new(storage: &str) -> Self {
        Self {
            storage: PathBuf::from(storage),
        }
    }

    pub fn push(&mut self, id: &str) {
        let mut file = OpenOptions::new()
            .create(true)
            .append(true)
            .open(&self.storage)
            .expect("Could not open registry");

        writeln!(file, "{}", id).unwrap();

        if self.registry_file_len() == IDLEN + 1 {
            dzen2::on().unwrap();
        }
    }

    pub fn pop(&self) -> Option<String> {
        if let Ok(file) = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&self.storage)
        {
            let lines = BufReader::new(&file).lines().flatten();

            if let Some(last_id) = lines.last() {
                let new_length = self.registry_file_len() - last_id.len() as u64 - 1;
                file.set_len(new_length).unwrap();

                if new_length == 0 {
                    dzen2::off().unwrap();
                }

                return Some(last_id);
            }
        };
        None
    }

    pub fn shift(&self) -> Option<String> {
        use std::collections::VecDeque;

        if let Ok(mut file) = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&self.storage)
        {
            let mut ids: VecDeque<_> = BufReader::new(&file).lines().flatten().collect();

            if let Some(first_id) = ids.pop_front() {
                file.set_len(0).unwrap();
                file.seek(SeekFrom::Start(0)).unwrap();
                if ids.is_empty() {
                    dzen2::off().unwrap();
                } else {
                    for id in ids {
                        writeln!(file, "{}", id).unwrap();
                    }
                }
                return Some(first_id);
            }
        };
        None
    }

    pub fn swap_with_last(&self, id: &str) -> io::Result<String> {
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&self.storage)?;

        let mut reader = BufReader::new(&file);
        let mut last_id = [0; IDLEN as usize];

        reader.seek(SeekFrom::End(IDSHIFT))?;
        reader.read_exact(&mut last_id)?;

        let mut writer = BufWriter::new(&file);
        writer.seek(SeekFrom::End(IDSHIFT))?;
        writer.write_all(id.as_bytes())?;

        Ok(String::from_utf8(last_id.to_vec()).unwrap())
    }

    fn registry_file_len(&self) -> u64 {
        self.storage.metadata().unwrap().len()
    }
}
